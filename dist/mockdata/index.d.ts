export { insertParams, selectParams, deleteParams, updateParams } from './services.params';
export { default as physicalEntityMockedDynamic } from './physical.entity.dynamic';
export { default as physicalEntityMockedRDBMS } from './physical.entity.rdbms';
//# sourceMappingURL=index.d.ts.map