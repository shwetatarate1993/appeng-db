"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var services_params_1 = require("./services.params");
exports.insertParams = services_params_1.insertParams;
exports.selectParams = services_params_1.selectParams;
exports.deleteParams = services_params_1.deleteParams;
exports.updateParams = services_params_1.updateParams;
var physical_entity_dynamic_1 = require("./physical.entity.dynamic");
exports.physicalEntityMockedDynamic = physical_entity_dynamic_1.default;
var physical_entity_rdbms_1 = require("./physical.entity.rdbms");
exports.physicalEntityMockedRDBMS = physical_entity_rdbms_1.default;
//# sourceMappingURL=index.js.map