import { ServiceOrchestrator } from '../services';
export default class AppengDBConfig {
    static readonly INSTANCE: ServiceOrchestrator;
    static configure(config: any, configuredDialect: string): void;
    private static serviceOrchestrator;
    private constructor();
}
//# sourceMappingURL=appeng.db.config.d.ts.map