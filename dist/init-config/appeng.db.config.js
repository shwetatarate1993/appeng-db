"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const services_1 = require("../services");
class AppengDBConfig {
    constructor() {
        /** No Op */
    }
    static get INSTANCE() {
        return AppengDBConfig.serviceOrchestrator;
    }
    static configure(config, configuredDialect) {
        if (AppengDBConfig.serviceOrchestrator === undefined ||
            AppengDBConfig.serviceOrchestrator === null) {
            const serviceOrchestrator = services_1.createServiceOrchestrator(config, configuredDialect);
            AppengDBConfig.serviceOrchestrator = serviceOrchestrator;
        }
    }
}
exports.default = AppengDBConfig;
//# sourceMappingURL=appeng.db.config.js.map