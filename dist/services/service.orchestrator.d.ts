import { DatabaseValidation, DBOperation, PhysicalEntity } from '../models';
export declare class ServiceOrchestrator {
    private config;
    private configuredDialect;
    constructor(config: any, configuredDialect: string);
    insert(physicalEntity: PhysicalEntity, params: any): Promise<string | number>;
    update(physicalEntity: PhysicalEntity, params: any): Promise<number>;
    delete(physicalEntity: PhysicalEntity, params: any): Promise<number>;
    selectSingleRecord(physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectRecords(physicalEntity: PhysicalEntity, params: any): Promise<any>;
    createSequence(physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectGridRecords(dbOperation: DBOperation, params: any): Promise<any>;
    selectRecordForValidationObject(validationObject: DatabaseValidation, params: any): Promise<any>;
    selectSingleRecordUsingQuery(datasourceKey: string, query: string, params: any): Promise<any>;
    selectRecordsUsingQuery(datasourceKey: string, query: string, params: any): Promise<any>;
}
declare const createServiceOrchestrator: (config: any, configuredDialect: string) => ServiceOrchestrator;
export default createServiceOrchestrator;
//# sourceMappingURL=service.orchestrator.d.ts.map