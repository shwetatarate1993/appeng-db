"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_data_provider_1 = __importDefault(require("../provider/app.data.provider"));
class ServiceOrchestrator {
    constructor(config, configuredDialect) {
        this.config = config;
        this.configuredDialect = configuredDialect;
    }
    async insert(physicalEntity, params) {
        return app_data_provider_1.default.insert(this.config, this.configuredDialect, physicalEntity, params);
    }
    async update(physicalEntity, params) {
        return app_data_provider_1.default.update(this.config, this.configuredDialect, physicalEntity, params);
    }
    async delete(physicalEntity, params) {
        return app_data_provider_1.default.delete(this.config, this.configuredDialect, physicalEntity, params);
    }
    async selectSingleRecord(physicalEntity, params) {
        return app_data_provider_1.default.selectSingleRecord(this.config, this.configuredDialect, physicalEntity, params);
    }
    async selectRecords(physicalEntity, params) {
        return app_data_provider_1.default.selectRecords(this.config, this.configuredDialect, physicalEntity, params);
    }
    async createSequence(physicalEntity, params) {
        return app_data_provider_1.default.createSequence(this.config, this.configuredDialect, physicalEntity, params);
    }
    async selectGridRecords(dbOperation, params) {
        return app_data_provider_1.default.selectGridRecords(this.config, this.configuredDialect, dbOperation, params);
    }
    async selectRecordForValidationObject(validationObject, params) {
        return app_data_provider_1.default.selectRecordForValidationObject(this.config, this.configuredDialect, validationObject, params);
    }
    async selectSingleRecordUsingQuery(datasourceKey, query, params) {
        return app_data_provider_1.default.selectSingleRecordUsingQuery(this.config, this.configuredDialect, datasourceKey, query, params);
    }
    async selectRecordsUsingQuery(datasourceKey, query, params) {
        return app_data_provider_1.default.selectRecordsUsingQuery(this.config, this.configuredDialect, datasourceKey, query, params);
    }
}
exports.ServiceOrchestrator = ServiceOrchestrator;
let serviceOrchestrator;
const createServiceOrchestrator = (config, configuredDialect) => {
    if (serviceOrchestrator === undefined || serviceOrchestrator === null) {
        serviceOrchestrator = new ServiceOrchestrator(config, configuredDialect);
        // Object.freeze(serviceOrchestrator);
    }
    return serviceOrchestrator;
};
exports.default = createServiceOrchestrator;
//# sourceMappingURL=service.orchestrator.js.map