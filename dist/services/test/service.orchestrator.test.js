"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const constants_1 = require("../../constants");
const mockdata_1 = require("../../mockdata");
const service_orchestrator_1 = __importDefault(require("../service.orchestrator"));
const knex = knex_config_1.knexClient(info_commons_1.config.get('db'), constants_1.MOCK, 'sqlite3');
const serviceOrchestrator = service_orchestrator_1.default(info_commons_1.config.get('db'), 'sqlite3');
const deleteCompanyTableRecords = async () => {
    return await knex('Company').del();
};
const createCompanyTable = async () => {
    return knex.schema.createTableIfNotExists('Company', (table) => {
        table.integer('ID');
        table.string('NAME');
        table.string('PLACE');
        table.string('CITY');
        table.string('COUNTRY');
        table.timestamp('CURR_DATE');
    });
};
const tableSetup = async () => {
    const tableCreated = await createCompanyTable();
    const recordsDeleted = await deleteCompanyTableRecords();
};
test('services for knex data provider', async () => {
    try {
        await tableSetup();
        await serviceOrchestrator.insert(mockdata_1.physicalEntityMockedDynamic[0], mockdata_1.insertParams);
        const record = await serviceOrchestrator.selectSingleRecord(mockdata_1.physicalEntityMockedDynamic[0], mockdata_1.insertParams);
        expect(mockdata_1.insertParams.ID === record.ID).toBe(true);
        expect(record.NAME === 'xyz').toBe(true);
        await serviceOrchestrator.update(mockdata_1.physicalEntityMockedDynamic[0], mockdata_1.updateParams);
        const records = await serviceOrchestrator.selectRecords(mockdata_1.physicalEntityMockedDynamic[0], mockdata_1.insertParams);
        expect(records[0].NAME === 'abc').toBe(true);
        await serviceOrchestrator.delete(mockdata_1.physicalEntityMockedDynamic[0], mockdata_1.updateParams);
    }
    catch (error) {
        throw error;
    }
});
test('services for mysql data provider', async () => {
    try {
        const insertResponse = await serviceOrchestrator.insert(mockdata_1.physicalEntityMockedRDBMS[0], mockdata_1.insertParams);
        expect(mockdata_1.insertParams.ID === insertResponse).toBe(true);
        const record = await serviceOrchestrator.selectSingleRecord(mockdata_1.physicalEntityMockedRDBMS[0], mockdata_1.updateParams);
        const records = await serviceOrchestrator.selectRecords(mockdata_1.physicalEntityMockedRDBMS[0], mockdata_1.insertParams);
        expect(mockdata_1.insertParams.ID === record.ID).toBe(true);
        const sequence = await serviceOrchestrator.createSequence(mockdata_1.physicalEntityMockedRDBMS[0], mockdata_1.insertParams);
        expect(sequence.ID === mockdata_1.insertParams.ID + 1).toBe(true);
        await serviceOrchestrator.update(mockdata_1.physicalEntityMockedRDBMS[0], mockdata_1.updateParams);
        await serviceOrchestrator.delete(mockdata_1.physicalEntityMockedRDBMS[0], mockdata_1.updateParams);
    }
    catch (error) {
        throw error;
    }
});
//# sourceMappingURL=service.orchestrator.test.js.map