"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const data_provider_1 = require("../../provider/data.provider");
const knex = knex_config_1.knexClient(info_commons_1.config.get('db'), 'METADB', 'mysql');
const fixedUUID = '03f66719-e1c0-11e9-8548-0ee72c6ddce6';
const query = 'SELECT * FROM vpmd.CONFIGITEMPROPERTY where propertyname=\'VALIDATION_QID\' and itemid' +
    ' in(SELECT itemid FROM vpmd.CONFIGITEM where PROJECTID=0) and ISDELETED=0 and PROPERTYVALUE!=\'0\'' +
    ' and PROPERTYVALUE is not null';
test('grid test', async () => {
    const fetchedRecords = await knex.raw(query);
    const records = fetchedRecords[0];
    // console.log("records...", records);
    for (const gridQuery of records) {
        // let formData = {
        //     "APP_LOGGED_IN_YEAR": "2017-2018", "APP_LOGGED_IN_ROLE_ID": 17,
        //     "APP_LOGGED_IN_USER_GP": 519, "ISDELETED": 0,
        //     "APP_LOGGED_IN_USER_TALUKA":27
        // };
        // formData = putMissingdbCodesinParams(gridQuery.PROPERTYVALUE,formData);
        // console.log("formData...",formData)
        try {
            const returnData = await data_provider_1.rdbmsDataProvider.selectRecordsWithoutPhysicalEntity(info_commons_1.config.get('db'), 'sqlite3', 'PRIMARYSPRING', gridQuery.PROPERTYVALUE, {});
        }
        catch (error) {
            console.log('error...', error, gridQuery.PROPERTYID);
        }
    }
});
const putMissingdbCodesinParams = (query1, params) => {
    let dbCodeList = [];
    dbCodeList = query1.match(/:\w+/g);
    if (dbCodeList !== null) {
        dbCodeList.map((dbCode) => {
            const column = dbCode.replace(':', '').trim();
            if (params[column] === undefined) {
                if (column.includes('_UUID')) {
                    params[column] = fixedUUID;
                }
                else {
                    params[column] = null;
                }
            }
        });
    }
    return params;
};
//# sourceMappingURL=gridquery.execute.test1.js.map