"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../../constants");
exports.parseUpdateOrDeleteResponseToGeneric = (response, dialect) => {
    switch (dialect) {
        case constants_1.Dialect.MYSQL:
            return response[0].affectedRows;
        case constants_1.Dialect.SQLITE:
            return -1;
    }
};
exports.parseSingleSelectResponseToGeneric = (response, dialect) => {
    switch (dialect) {
        case constants_1.Dialect.MYSQL:
            let records = [];
            records = response[0];
            return records.length > 0 ? records[0] : {};
        case constants_1.Dialect.SQLITE:
            return response.length > 0 ? response[0] : {};
    }
};
exports.parseSelectResponseToGeneric = (response, dialect) => {
    switch (dialect) {
        case constants_1.Dialect.MYSQL:
            return response[0];
        case constants_1.Dialect.SQLITE:
            return response;
    }
};
//# sourceMappingURL=parse.response.generic.js.map