import { DBOperation, PhysicalEntity } from '../../models';
import { IDataProvider } from '../idata.provider';
export declare class RDBMSDataProvider implements IDataProvider {
    insert(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, inParam: any): Promise<number | string>;
    update(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, inParam: any): Promise<number>;
    delete(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<number>;
    selectSingleRecord(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, inParam: any): Promise<any>;
    selectRecords(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, inParam: any): Promise<any>;
    createSequence(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectGridRecords(config: any, configuredDialect: string, dbOperation: DBOperation, params: any): Promise<any>;
    selectSingleRecordWithoutPhysicalEntity(config: any, configuredDialect: string, datasourceKey: string, inQuery: string, inParam: any): Promise<any>;
    selectRecordsWithoutPhysicalEntity(config: any, configuredDialect: string, datasourceKey: string, inQuery: string, inParam: any): Promise<any>;
}
declare const rdbmsDataProvider: RDBMSDataProvider;
export default rdbmsDataProvider;
//# sourceMappingURL=rdbms.data.provider.d.ts.map