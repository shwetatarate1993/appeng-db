"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const knex_config_1 = require("knex-config");
const appeng_utils_1 = require("../../utilities/appeng.utils");
class DynamicDataProvider {
    async insert(config, configuredDialect, physicalEntity, inParams) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const tableName = physicalEntity.dbtypeName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        const dbCodeList = appeng_utils_1.getdbCodeList(physicalEntity.physicalColumns);
        const primarydbCodeKey = appeng_utils_1.getPrimarydbCode(physicalEntity.physicalColumns);
        const params = appeng_utils_1.removeUnneccesaryDataFromParams(inParams, dbCodeList);
        try {
            await knex(tableName).insert(params);
        }
        catch (error) {
            throw error;
        }
        return params[primarydbCodeKey];
    }
    async update(config, configuredDialect, physicalEntity, inParams) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const tableName = physicalEntity.dbtypeName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        const dbCodeList = appeng_utils_1.getdbCodeList(physicalEntity.physicalColumns);
        const primarydbCodeKey = appeng_utils_1.getPrimarydbCode(physicalEntity.physicalColumns);
        const params = appeng_utils_1.removeUnneccesaryDataFromParams(inParams, dbCodeList);
        let response;
        try {
            response = await knex(tableName).update(params)
                .where(primarydbCodeKey, params[primarydbCodeKey]);
        }
        catch (error) {
            throw error;
        }
        return response;
    }
    async delete(config, configuredDialect, physicalEntity, params) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const tableName = physicalEntity.dbtypeName;
        const primarydbCodeKey = appeng_utils_1.getPrimarydbCode(physicalEntity.physicalColumns);
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let response;
        try {
            response = await knex(tableName)
                .where(primarydbCodeKey, params[primarydbCodeKey]).del();
        }
        catch (error) {
            throw error;
        }
        return response;
    }
    async selectSingleRecord(config, configuredDialect, physicalEntity, params) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const tableName = physicalEntity.dbtypeName;
        const primarydbCodeKey = appeng_utils_1.getPrimarydbCode(physicalEntity.physicalColumns);
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let response;
        try {
            response = await knex.table(tableName).first()
                .where(primarydbCodeKey, params[primarydbCodeKey]);
        }
        catch (error) {
            throw error;
        }
        return response;
    }
    async selectRecords(config, configuredDialect, physicalEntity, params) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const tableName = physicalEntity.dbtypeName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let response;
        try {
            response = await knex.table(tableName).select();
        }
        catch (error) {
            throw error;
        }
        return response;
    }
    async createSequence(config, configuredDialect, physicalEntity, params) {
        throw new Error(('Implementation not avaiable'));
    }
    async selectGridRecords(config, configuredDialect, dbOperation, params) {
        throw new Error(('Implementation not avaiable'));
    }
    async selectSingleRecordWithoutPhysicalEntity(config, configuredDialect, datasourceKey, inQuery, inParam) {
        throw new Error(('Implementation not avaiable'));
    }
}
exports.DynamicDataProvider = DynamicDataProvider;
const dynamicDataProvider = new DynamicDataProvider();
Object.freeze(dynamicDataProvider);
exports.default = dynamicDataProvider;
//# sourceMappingURL=dynamic.data.provider.js.map