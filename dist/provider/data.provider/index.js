"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rdbms_data_provider_1 = require("./rdbms.data.provider");
exports.rdbmsDataProvider = rdbms_data_provider_1.default;
var dynamic_data_provider_1 = require("./dynamic.data.provider");
exports.dynamicDataProvider = dynamic_data_provider_1.default;
var parse_response_generic_1 = require("./parse.response.generic");
exports.parseSelectResponseToGeneric = parse_response_generic_1.parseSelectResponseToGeneric;
exports.parseSingleSelectResponseToGeneric = parse_response_generic_1.parseSingleSelectResponseToGeneric;
exports.parseUpdateOrDeleteResponseToGeneric = parse_response_generic_1.parseUpdateOrDeleteResponseToGeneric;
//# sourceMappingURL=index.js.map