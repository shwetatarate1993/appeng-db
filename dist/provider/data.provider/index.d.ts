export { default as rdbmsDataProvider } from './rdbms.data.provider';
export { default as dynamicDataProvider } from './dynamic.data.provider';
export { parseSelectResponseToGeneric, parseSingleSelectResponseToGeneric, parseUpdateOrDeleteResponseToGeneric, } from './parse.response.generic';
//# sourceMappingURL=index.d.ts.map