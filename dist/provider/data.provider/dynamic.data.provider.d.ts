import { DBOperation, PhysicalEntity } from '../../models';
import { IDataProvider } from '../idata.provider';
export declare class DynamicDataProvider implements IDataProvider {
    insert(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, inParams: any): Promise<number | string>;
    update(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, inParams: any): Promise<number>;
    delete(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<number>;
    selectSingleRecord(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectRecords(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    createSequence(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectGridRecords(config: any, configuredDialect: string, dbOperation: DBOperation, params: any): Promise<any>;
    selectSingleRecordWithoutPhysicalEntity(config: any, configuredDialect: string, datasourceKey: string, inQuery: string, inParam: any): Promise<any>;
}
declare const dynamicDataProvider: DynamicDataProvider;
export default dynamicDataProvider;
//# sourceMappingURL=dynamic.data.provider.d.ts.map