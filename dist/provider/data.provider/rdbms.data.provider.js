"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const knex_config_1 = require("knex-config");
const appeng_utils_1 = require("../../utilities/appeng.utils");
const _1 = require("./");
class RDBMSDataProvider {
    async insert(config, configuredDialect, physicalEntity, inParam) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let query = info_commons_1.queryExtractor(physicalEntity.insertQID, configuredDialect);
        const params = info_commons_1.putMissingdbCodesinParams(query, inParam);
        if (query.includes('</if>')) {
            query = info_commons_1.queryParser(query, params);
        }
        const primarydbCodeKey = appeng_utils_1.getPrimarydbCode(physicalEntity.physicalColumns);
        try {
            await knex.raw(query, params);
        }
        catch (error) {
            throw error;
        }
        return params[primarydbCodeKey];
    }
    async update(config, configuredDialect, physicalEntity, inParam) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let query = info_commons_1.queryExtractor(physicalEntity.updateQID, configuredDialect);
        const params = info_commons_1.putMissingdbCodesinParams(query, inParam);
        if (query.includes('</if>')) {
            query = info_commons_1.queryParser(query, params);
        }
        let response;
        try {
            response = await knex.raw(query, params);
        }
        catch (error) {
            throw error;
        }
        return _1.parseUpdateOrDeleteResponseToGeneric(response, configuredDialect);
    }
    async delete(config, configuredDialect, physicalEntity, params) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        const query = info_commons_1.queryExtractor(physicalEntity.deleteQID, configuredDialect);
        let response;
        try {
            response = await knex.raw(query, params);
        }
        catch (error) {
            throw error;
        }
        return _1.parseUpdateOrDeleteResponseToGeneric(response, configuredDialect);
    }
    async selectSingleRecord(config, configuredDialect, physicalEntity, inParam) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let query = info_commons_1.queryExtractor(physicalEntity.singleSelectQID, configuredDialect);
        const params = info_commons_1.putMissingdbCodesinParams(query, inParam);
        if (query.includes('</if>')) {
            query = info_commons_1.queryParser(query, params);
        }
        let response;
        try {
            response = await knex.raw(query, params);
        }
        catch (error) {
            throw error;
        }
        return _1.parseSingleSelectResponseToGeneric(response, configuredDialect);
    }
    async selectRecords(config, configuredDialect, physicalEntity, inParam) {
        const datasourceKey = physicalEntity.workAreaSessName;
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let query = info_commons_1.queryExtractor(physicalEntity.multiSelectQID, configuredDialect);
        const params = info_commons_1.putMissingdbCodesinParams(query, inParam);
        if (query.includes('</if>')) {
            query = info_commons_1.queryParser(query, params);
        }
        let response;
        try {
            response = await knex.raw(query, params);
        }
        catch (error) {
            throw error;
        }
        return _1.parseSelectResponseToGeneric(response, configuredDialect);
    }
    async createSequence(config, configuredDialect, physicalEntity, params) {
        return this.selectSingleRecordWithoutPhysicalEntity(config, configuredDialect, physicalEntity.workAreaSessName, physicalEntity.sequenceQID, params);
    }
    async selectGridRecords(config, configuredDialect, dbOperation, params) {
        return this.selectRecordsWithoutPhysicalEntity(config, configuredDialect, dbOperation.workAreaSessName, dbOperation.gridSelectId, params);
    }
    async selectSingleRecordWithoutPhysicalEntity(config, configuredDialect, datasourceKey, inQuery, inParam) {
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let query = info_commons_1.queryExtractor(inQuery, configuredDialect);
        const params = info_commons_1.putMissingdbCodesinParams(query, inParam);
        if (query.includes('</if>')) {
            query = info_commons_1.queryParser(query, params);
        }
        let response;
        try {
            response = await knex.raw(query, params);
        }
        catch (error) {
            throw error;
        }
        return _1.parseSingleSelectResponseToGeneric(response, configuredDialect);
    }
    async selectRecordsWithoutPhysicalEntity(config, configuredDialect, datasourceKey, inQuery, inParam) {
        const knex = knex_config_1.knexClient(config, datasourceKey, configuredDialect);
        let query = info_commons_1.queryExtractor(inQuery, configuredDialect);
        const params = info_commons_1.putMissingdbCodesinParams(query, inParam);
        if (query.includes('</if>')) {
            query = info_commons_1.queryParser(query, params);
        }
        let response;
        try {
            response = await knex.raw(query, params);
        }
        catch (error) {
            throw error;
        }
        return _1.parseSelectResponseToGeneric(response, configuredDialect);
    }
}
exports.RDBMSDataProvider = RDBMSDataProvider;
const rdbmsDataProvider = new RDBMSDataProvider();
Object.freeze(rdbmsDataProvider);
exports.default = rdbmsDataProvider;
//# sourceMappingURL=rdbms.data.provider.js.map