export declare const parseUpdateOrDeleteResponseToGeneric: (response: any, dialect: string) => any;
export declare const parseSingleSelectResponseToGeneric: (response: any, dialect: string) => any;
export declare const parseSelectResponseToGeneric: (response: any, dialect: string) => any;
//# sourceMappingURL=parse.response.generic.d.ts.map