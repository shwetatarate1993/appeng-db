import { DatabaseValidation, DBOperation, PhysicalEntity } from '../models';
import { IDataProvider } from './idata.provider';
export declare class AppDataProvider implements IDataProvider {
    insert(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<string | number>;
    update(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<number>;
    delete(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<number>;
    selectSingleRecord(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectRecords(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    createSequence(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectGridRecords(config: any, configuredDialect: string, dbOperation: DBOperation, params: any): Promise<any>;
    selectRecordForValidationObject(config: any, configuredDialect: string, validationObject: DatabaseValidation, params: any): Promise<any>;
    selectSingleRecordUsingQuery(config: any, configuredDialect: string, datasourceKey: string, query: string, params: any): Promise<any>;
    selectRecordsUsingQuery(config: any, configuredDialect: string, datasourceKey: string, query: string, params: any): Promise<any>;
}
declare const appDataProvider: AppDataProvider;
export default appDataProvider;
//# sourceMappingURL=app.data.provider.d.ts.map