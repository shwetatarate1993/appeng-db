import { DBOperation, PhysicalEntity } from '../models';
export interface IDataProvider {
    insert(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<number | string>;
    update(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<number>;
    delete(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<number>;
    selectSingleRecord(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectRecords(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    createSequence(config: any, configuredDialect: string, physicalEntity: PhysicalEntity, params: any): Promise<any>;
    selectGridRecords(config: any, configuredDialect: string, dbOperation: DBOperation, params: any): Promise<any>;
}
//# sourceMappingURL=idata.provider.d.ts.map