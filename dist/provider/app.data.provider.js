'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("../constants");
const appeng_utils_1 = require("../utilities/appeng.utils");
const data_provider_1 = require("./data.provider");
class AppDataProvider {
    async insert(config, configuredDialect, physicalEntity, params) {
        switch (appeng_utils_1.getDatasourceType(physicalEntity.insertQID)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.insert(config, configuredDialect, physicalEntity, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.insert(config, configuredDialect, physicalEntity, params);
        }
    }
    async update(config, configuredDialect, physicalEntity, params) {
        switch (appeng_utils_1.getDatasourceType(physicalEntity.updateQID)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.update(config, configuredDialect, physicalEntity, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.update(config, configuredDialect, physicalEntity, params);
        }
    }
    async delete(config, configuredDialect, physicalEntity, params) {
        switch (appeng_utils_1.getDatasourceType(physicalEntity.deleteQID)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.delete(config, configuredDialect, physicalEntity, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.delete(config, configuredDialect, physicalEntity, params);
        }
    }
    async selectSingleRecord(config, configuredDialect, physicalEntity, params) {
        switch (appeng_utils_1.getDatasourceType(physicalEntity.singleSelectQID)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.selectSingleRecord(config, configuredDialect, physicalEntity, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.selectSingleRecord(config, configuredDialect, physicalEntity, params);
        }
    }
    async selectRecords(config, configuredDialect, physicalEntity, params) {
        switch (appeng_utils_1.getDatasourceType(physicalEntity.multiSelectQID)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.selectRecords(config, configuredDialect, physicalEntity, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.selectRecords(config, configuredDialect, physicalEntity, params);
        }
    }
    async createSequence(config, configuredDialect, physicalEntity, params) {
        switch (appeng_utils_1.getDatasourceType(physicalEntity.sequenceQID)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.createSequence(config, configuredDialect, physicalEntity, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.createSequence(config, configuredDialect, physicalEntity, params);
        }
    }
    async selectGridRecords(config, configuredDialect, dbOperation, params) {
        switch (appeng_utils_1.getDatasourceType(dbOperation.gridSelectId)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.selectGridRecords(config, configuredDialect, dbOperation, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.selectGridRecords(config, configuredDialect, dbOperation, params);
        }
    }
    async selectRecordForValidationObject(config, configuredDialect, validationObject, params) {
        switch (appeng_utils_1.getDatasourceType(validationObject.validationQid)) {
            case constants_1.DataSourceType.RDBMS:
                return data_provider_1.rdbmsDataProvider.selectSingleRecordWithoutPhysicalEntity(config, configuredDialect, validationObject.datasourceName, validationObject.validationQid, params);
            case constants_1.DataSourceType.DYNAMIC:
                return data_provider_1.dynamicDataProvider.selectSingleRecordWithoutPhysicalEntity(config, configuredDialect, validationObject.datasourceName, validationObject.validationQid, params);
        }
    }
    async selectSingleRecordUsingQuery(config, configuredDialect, datasourceKey, query, params) {
        return data_provider_1.rdbmsDataProvider.selectSingleRecordWithoutPhysicalEntity(config, configuredDialect, datasourceKey, query, params);
    }
    async selectRecordsUsingQuery(config, configuredDialect, datasourceKey, query, params) {
        return data_provider_1.rdbmsDataProvider.selectRecordsWithoutPhysicalEntity(config, configuredDialect, datasourceKey, query, params);
    }
}
exports.AppDataProvider = AppDataProvider;
const appDataProvider = new AppDataProvider();
Object.freeze(appDataProvider);
exports.default = appDataProvider;
//# sourceMappingURL=app.data.provider.js.map