import { DataSourceType } from '../constants';
import { PhysicalColumn } from '../models';
export declare const removeUnneccesaryDataFromParams: (inParams: any, columnList: string[]) => any;
export declare const getdbCodeList: (physicalColumn: PhysicalColumn[]) => string[];
export declare const getPrimarydbCode: (physicalColumn: PhysicalColumn[]) => string;
export declare const getDatasourceType: (query: string) => DataSourceType.DYNAMIC | DataSourceType.RDBMS;
//# sourceMappingURL=appeng.utils.d.ts.map