"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
const constants_1 = require("../constants");
exports.removeUnneccesaryDataFromParams = (inParams, columnList) => {
    const params = cloneDeep_1.default(inParams);
    Object.entries(params).forEach(([key, value]) => {
        if (!columnList.includes(key)) {
            delete params[key];
        }
    });
    return params;
};
exports.getdbCodeList = (physicalColumn) => {
    const dbCodeList = [];
    physicalColumn.map((column) => {
        if (column.isVirtual === undefined || column.isVirtual === null
            || !column.isVirtual) {
            dbCodeList.push(column.dbCode);
        }
    });
    return dbCodeList;
};
exports.getPrimarydbCode = (physicalColumn) => {
    return physicalColumn.find((column) => column.isPrimaryKey).dbCode;
};
exports.getDatasourceType = (query) => {
    return query ? constants_1.DataSourceType.RDBMS : constants_1.DataSourceType.DYNAMIC;
};
//# sourceMappingURL=appeng.utils.js.map