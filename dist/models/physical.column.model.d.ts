export default class PhysicalColumn {
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly dataType: string;
    readonly length: number;
    readonly dbCode: string;
    readonly dbtypeName: string;
    readonly isPrimaryKey: boolean;
    readonly isVirtual: boolean;
    readonly isKey: boolean;
    readonly isPhysicalColumnMandatory: boolean;
    readonly isVerticalField: boolean;
    readonly isUnique: boolean;
    readonly isMandatory: boolean;
}
//# sourceMappingURL=physical.column.model.d.ts.map