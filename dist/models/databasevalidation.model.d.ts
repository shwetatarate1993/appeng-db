export default class DatabaseValidation {
    configObjectId: string;
    name: string;
    configObjectType: string;
    createdBy: string;
    isDeleted: number;
    itemDescription: string;
    creationDate: Date;
    projectId: number;
    updatedBy: string;
    updationDate: Date;
    deletionDate: Date;
    datasourceName: string;
    validationType: string;
    validationExpression: string;
    validationQid: string;
    validationMessage: string;
    validationExpressionKeys: string;
    privileges?: any[];
    childRelations?: any[];
    parentRelations?: any[];
    constructor(itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, datasourceName: string, validationType: string, validationExpression: string, validationQid: string, validationMessage: string, validationExpressionKeys: string, privileges?: any[], childRelations?: any[], parentRelations?: any[]);
}
//# sourceMappingURL=databasevalidation.model.d.ts.map