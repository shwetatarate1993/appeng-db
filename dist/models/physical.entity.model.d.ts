import PhysicalColumn from './physical.column.model';
export default class PhysicalEntity {
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly schemaName: string;
    readonly dbtypeName: string;
    readonly dbtype: string;
    readonly isVerticalEntity: boolean;
    readonly order: number;
    readonly singleSelectQID: string;
    readonly multiSelectQID: string;
    readonly updateQID: string;
    readonly deleteQID: string;
    readonly insertQID: string;
    readonly sequenceQID: string;
    readonly workAreaSessName: string;
    readonly releaseAreaSqlSessionfactoryName: string;
    readonly physicalColumns: PhysicalColumn[];
    readonly isPrimaryEntity: boolean;
}
//# sourceMappingURL=physical.entity.model.d.ts.map