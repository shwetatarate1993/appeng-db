'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class DatabaseValidation {
    constructor(itemId, itemName, itemType, projectId, createdBy, itemDescription, creationDate, updatedBy, updationDate, deletionDate, isDeleted, datasourceName, validationType, validationExpression, validationQid, validationMessage, validationExpressionKeys, privileges, childRelations, parentRelations) {
        this.configObjectId = itemId;
        this.name = itemName;
        this.configObjectType = itemType;
        this.projectId = projectId;
        this.createdBy = createdBy;
        this.itemDescription = itemDescription;
        this.creationDate = creationDate;
        this.updatedBy = updatedBy;
        this.updationDate = updationDate;
        this.deletionDate = deletionDate;
        this.isDeleted = isDeleted;
        this.datasourceName = datasourceName;
        this.validationType = validationType;
        this.validationExpression = validationExpression;
        this.validationQid = validationQid;
        this.validationMessage = validationMessage;
        this.validationExpressionKeys = validationExpressionKeys;
        this.privileges = privileges;
        this.childRelations = childRelations;
        this.parentRelations = parentRelations;
        Object.freeze(this);
    }
}
exports.default = DatabaseValidation;
//# sourceMappingURL=databasevalidation.model.js.map