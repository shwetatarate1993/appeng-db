export default class DBOperation {
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly selectId: string;
    readonly gridSelectId: string;
    readonly filteredGridSelectQuery: string;
    readonly workAreaSessName: string;
    readonly releaseAreaSqlSessionfactoryName: string;
}
//# sourceMappingURL=db.operation.model.d.ts.map