export declare enum DataSourceType {
    DYNAMIC = "DYNAMIC",
    RDBMS = "RDBMS",
    SPRINGSQL = "SPRINGSQL"
}
export declare enum Dialect {
    SQLITE = "sqlite3",
    MYSQL = "mysql"
}
//# sourceMappingURL=appeng.enum.d.ts.map