"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DataSourceType;
(function (DataSourceType) {
    DataSourceType["DYNAMIC"] = "DYNAMIC";
    DataSourceType["RDBMS"] = "RDBMS";
    DataSourceType["SPRINGSQL"] = "SPRINGSQL";
})(DataSourceType = exports.DataSourceType || (exports.DataSourceType = {}));
var Dialect;
(function (Dialect) {
    Dialect["SQLITE"] = "sqlite3";
    Dialect["MYSQL"] = "mysql";
})(Dialect = exports.Dialect || (exports.Dialect = {}));
//# sourceMappingURL=appeng.enum.js.map